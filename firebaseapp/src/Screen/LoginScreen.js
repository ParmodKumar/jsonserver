import {
  KeyboardAvoidingView,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/core';

import React, {useEffect, useState} from 'react';
import {authention} from '../../firebase';
import {
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  onAuthStateChanged,
} from 'firebase/auth';

const LoginScreen = () => {
  const navigation = useNavigation();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [err, seterror] = useState('');

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(authention, user => {
      if (user) {
        navigation.replace('Home');
      } else {
      }
    });
    return unsubscribe;
  }, []);

  const handleSignUp = () => {
    createUserWithEmailAndPassword(authention, email, password)
      .then(userCredentials => {
        const user = userCredentials.user;
        console.log('Registered with:', user.email);
      })
      .catch(error => alert(error.message));
  };

  const handLogin = () => {
    signInWithEmailAndPassword(authention, email, password)
      .catch(() => {
        createUserWithEmailAndPassword(authention, email, password);
      })
      .catch(e => {
        seterror('Authention HA');
      });
  };
  // signInWithEmailAndPassword(authention, email, password)
  // .catch(() => {
  //   createUserWithEmailAndPassword(authention, email, password);
  // })
  // .catch(e => {
  //   seterror('Authention HA');
  // });
  // createUserWithEmailAndPassword(authention, email, password)
  //   .then(re => {
  //     console.log(re);
  //     setIsSignedIn(true);
  //   })
  //   .catch(e => {
  //     console.log(e);
  //   });
  // createUserWithEmailAndPassword(authention, email, password)
  // .then(userCredentials => {
  //   const user = userCredentials.user;
  //   console.log('Registered with:', user.email);
  // })
  // .catch(error => alert(error.message));
  return (
    <React.Fragment>
      <ScrollView>
        <KeyboardAvoidingView style={styles.container} behavior="padding">
          <View style={styles.inputContainer}>
            <TextInput
              placeholder="Email"
              value={email}
              onChangeText={text => setEmail(text)}
              style={styles.input}
            />
            <TextInput
              placeholder="Password"
              value={password}
              onChangeText={text => setPassword(text)}
              style={styles.input}
              secureTextEntry
            />
          </View>
          <Text style={styles.errorstyle}>{err}</Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={handLogin} style={styles.button}>
              <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={handleSignUp}
              style={[styles.button, styles.buttonOutline]}>
              <Text style={styles.buttonOutlineText}>Register</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </React.Fragment>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    width: '80%',
  },
  input: {
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 5,
  },
  buttonContainer: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  button: {
    backgroundColor: '#0782F9',
    width: '100%',
    padding: 15,
    borderRadius: 10,
    alignItems: 'center',
  },
  buttonOutline: {
    backgroundColor: 'white',
    marginTop: 5,
    borderColor: '#0782F9',
    borderWidth: 2,
  },
  buttonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
  },
  buttonOutlineText: {
    color: '#0782F9',
    fontWeight: '700',
    fontSize: 16,
  },
  errorstyle: {
    fontSize: 30,
    alignItems: 'center',
    color: 'red',
  },
});
