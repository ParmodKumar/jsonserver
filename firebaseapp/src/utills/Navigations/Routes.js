import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SignupScreen from '../../Screen/LoginScreen';
import Home from '../../Screen/Home';
const Stack = createNativeStackNavigator();

export const authStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="LoginScreen" component={SignupScreen} />
      <Stack.Screen name="Home" component={Home} />
    </Stack.Navigator>
  );
};
