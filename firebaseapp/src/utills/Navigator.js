import {NavigationContainer} from '@react-navigation/native';
import * as React from 'react';
import {authStack} from './Navigations/Routes';

export const Navigator = () => {
  return <NavigationContainer>{authStack()}</NavigationContainer>;
};
